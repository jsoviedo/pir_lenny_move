#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from math import pi
import tf_conversions
import lenny_move_commands

# joint_move(self, points, group_name):
# task_move(self, points, group_name):
# joint_plan(self, points, group_name):
# task_plan(self, points, group_name):
# execute_plan(self, plan, group_name):

if __name__ == '__main__':

	print("Begin Lenny Pkg Test")	
	raw_input()
	lenny_move_test = lenny_move_commands.LennyMove()
	lenny_move_test.setting_planner()

	group_left = "arm_left"
	group_right = "arm_right"

	home = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
	approach_point = [0.5, 0.4, 1.2, 0.0, 0.0,-(180.0*pi)/180.0]
	approach_point_2 = [0.5, -0.4, 1.2, 0.0, 0.0,-(180.0*pi)/180.0]
	joint_point = [0.0, pi/2, 0.0, 0.0, 0.0, 0.0, 0.0]

	print("Begin Move Test")

	print("Go to approach point")
	raw_input()
	lenny_move_test.joint_move(home, group_left)	
	lenny_move_test.task_move(approach_point, group_left)

	print("Return to Home")
	raw_input()
	lenny_move_test.joint_move(home, group_left)	
	
	print("Finish Move Test")

	print("Begin Plan and Execute Test")

	print("Plan to approach point")
	raw_input()
	lenny_move_test.joint_move(home, group_right)
	task_plan = lenny_move_test.task_plan(approach_point_2, group_right)
	print("Execute plan to approach point")
	raw_input()
	lenny_move_test.execute_plan(task_plan, group_right)
	print("Plan to home position")
	raw_input()
	joint_plan = lenny_move_test.joint_plan(home, group_right)
	print("Execute plan to home position")
	raw_input()
	lenny_move_test.execute_plan(joint_plan, group_right)

	print("Finish Plan and Execute Test")
	
	#print("go to joint point")
	#raw_input()
	#lenny_move_test.joint_move(home, group_left)
	#lenny_move_test.joint_move(joint_point, group_left)

	#print("return to home")
	#raw_input()
	#lenny_move_test.joint_move(home, group_left)

	print("Finish Lenny Pkg Test")	

	

