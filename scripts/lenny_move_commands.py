#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from math import pi

import tf_conversions

def all_close(goal, actual, tolerance):
	"""
	Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
	@param: goal       A list of floats, a Pose or a PoseStamped
	@param: actual     A list of floats, a Pose or a PoseStamped
	@param: tolerance  A float
	@returns: bool
	"""
	all_equal = True
	if type(goal) is list:
		for index in range(len(goal)):
			if abs(actual[index] - goal[index]) > tolerance:
				return False

	elif type(goal) is geometry_msgs.msg.PoseStamped:
		return all_close(goal.pose, actual.pose, tolerance)

	elif type(goal) is geometry_msgs.msg.Pose:
		return all_close(pose_to_list(goal), pose_to_list(actual), tolerance)

	return True

class LennyMove(object):

	# initialize program 

	def __init__(self):
		super(LennyMove, self).__init__()
		moveit_commander.roscpp_initialize(sys.argv)
		rospy.init_node('lenny_move_node',anonymous=True)
		
		robot = moveit_commander.RobotCommander()
		scene = moveit_commander.PlanningSceneInterface()

		
		group_left = moveit_commander.MoveGroupCommander("arm_left")
		group_right = moveit_commander.MoveGroupCommander("arm_right")

		display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory, queue_size=20)

		planning_frame_left = group_left.get_planning_frame()
		eef_link_left = group_left.get_end_effector_link()
		
		planning_frame_right = group_right.get_planning_frame()
		eef_link_right =  group_right

		group_names = robot.get_group_names()

		self.robot = robot
		self.scene = scene
		self.group_names =  group_names
		self.display_trajectory_publisher = display_trajectory_publisher
		
		self.group_left = group_left
		self.planning_frame_left = planning_frame_left
		self.eef_link_left = eef_link_left

		self.group_right = group_right
		self.planning_frame_right = planning_frame_right
		self.eef_link_right = eef_link_right

	# Set planner parameters

	def setting_planner(self):

		group_left = self.group_left
		group_left.set_num_planning_attempts(5)
		group_left.set_goal_position_tolerance(0.001)
		group_left.set_planning_time(10.0)
		group_left.set_planner_id("RRTConnectConfigDefault")
		group_left.allow_replanning(True)
		self.group_left = group_left

		group_right = self.group_right
		group_right.set_num_planning_attempts(5)
		group_right.set_goal_position_tolerance(0.001)
		group_right.set_planning_time(10.0)
		group_right.set_planner_id("RRTConnectConfigDefault")
		group_right.allow_replanning(True)	
		self.group_right = group_right
	
	# Move in joint space

	def joint_move(self, points, group_name):

		if(group_name=="arm_left"): group = self.group_left
		if(group_name=="arm_right"): group = self.group_right

		joint_goal = group.get_current_joint_values()
		joint_goal[0] = points[0]
		joint_goal[1] = points[1]
		joint_goal[2] = points[2]
		joint_goal[3] = points[3]
		joint_goal[4] = points[4]
		joint_goal[5] = points[5]
		joint_goal[6] = points[6]

		group.go(joint_goal, wait=True)
		group.stop()
		if(group_name=="arm_left"): current_joints = self.group_left.get_current_joint_values()
		if(group_name=="arm_right"): current_joints = self.group_right.get_current_joint_values()
		return all_close(joint_goal, current_joints, 0.01)

	# Plan in joint space

	def joint_plan(self, points, group_name):

		if(group_name=="arm_left"): group = self.group_left
		if(group_name=="arm_right"): group = self.group_right

		joint_goal = group.get_current_joint_values()
		joint_goal[0] = points[0]
		joint_goal[1] = points[1]
		joint_goal[2] = points[2]
		joint_goal[3] = points[3]
		joint_goal[4] = points[4]
		joint_goal[5] = points[5]
		joint_goal[6] = points[6]
		
		plan=group.plan()	
		group.stop()

		return plan

	# Move in task space

	def task_move(self, points, group_name):
		
		if(group_name=="arm_left"): group = self.group_left
		if(group_name=="arm_right"): group = self.group_right
		
		pose_goal = geometry_msgs.msg.Pose()
		pose_goal.position.x = points[0]
		pose_goal.position.y = points[1]
		pose_goal.position.z = points[2]

		quaternion_pick = tf_conversions.transformations.quaternion_from_euler(points[3], points[4], points[5])
		pose_goal.orientation.x = quaternion_pick[0]
		pose_goal.orientation.y = quaternion_pick[1]
		pose_goal.orientation.z = quaternion_pick[2]
		pose_goal.orientation.w = quaternion_pick[3]
		
		group.set_pose_target(pose_goal)
		group.go(wait=True)
		group.stop()
		group.clear_pose_targets()

		if(group_name=="arm_left"):current_pose = self.group_left.get_current_pose().pose
		if(group_name=="arm_right"): current_pose = self.group_right.get_current_pose().pose

		return all_close(pose_goal, current_pose, 0.01)

	def task_plan(self, points, group_name):

		if(group_name=="arm_left"): group = self.group_left
		if(group_name=="arm_right"): group = self.group_right

		pose_goal = geometry_msgs.msg.Pose()
		pose_goal.position.x = points[0]
		pose_goal.position.y = points[1]
		pose_goal.position.z = points[2]

		quaternion_pick = tf_conversions.transformations.quaternion_from_euler(points[3], points[4], points[5])
		pose_goal.orientation.x = quaternion_pick[0]
		pose_goal.orientation.y = quaternion_pick[1]
		pose_goal.orientation.z = quaternion_pick[2]
		pose_goal.orientation.w = quaternion_pick[3]

		group.set_pose_target(pose_goal)
		plan = group.plan()
		group.stop()
		group.clear_pose_targets()
		return plan

	# Execute plan in joint or task space
	
	def execute_plan(self, plan, group_name):
		if(group_name=="arm_left"): group = self.group_left
		if(group_name=="arm_right"): group = self.group_right
		group.execute(plan, wait=True)















		
