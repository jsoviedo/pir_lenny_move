#!/usr/bin/env python

################################################################
#              CTAI-PUJ - Perception In Robotics               #
#                     Date:  26/07/2019                        #
# 									  lenny_move_server                        #
################################################################

from pir_lenny_move.srv import *
import sys
import copy
import rospy

import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from math import pi

import math 
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

import tf_conversions
import yaml
import os

class PIRLennyMoveServer(object):

	def __init__(self):
		super(PIRLennyMoveServer, self).__init__()
		moveit_commander.roscpp_initialize(sys.argv)
		rospy.init_node('pir_lenny_move_server_node', anonymous =True)
		robot = moveit_commander.RobotCommander()
		scene = moveit_commander.PlanningSceneInterface()

		# Groups avaible on lenny's pkgs:
		# torso, arm_left, arm right, arms and sda10f

		group_left = moveit_commander.MoveGroupCommander('arm_left')
		group_right = moveit_commander.MoveGroupCommander('arm_right')
		group_torso = moveit_commander.MoveGroupCommander('torso')

		display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)
		
		planning_frame_left =  group_left.get_planning_frame()
		eef_link_left =  group_left.get_end_effector_link()
		
		planning_frame_right = group_right.get_planning_frame()
		eef_link_right = group_right.get_end_effector_link()

		planning_frame_torso = group_torso.get_planning_frame()

		group_name = robot.get_group_names()

		self.robot = robot
		self.scene = scene
		self.group_name = group_name
		self.display_trajectory_publisher = display_trajectory_publisher

		self.group_left = group_left
		self.planning_frame_left = planning_frame_left
		self.eef_link_left = eef_link_left

		self.group_right = group_right
		self.planning_frame_right = planning_frame_right
		self.eef_link_right = eef_link_right

		self.group_torso = group_torso
		self.planning_frame_torso = planning_frame_torso

	def joint_move(self, req):

		group_name=req.GroupName
		if(group_name == "arm_left"): group = self.group_left
		if(group_name == "arm_right"): group = self.group_right

		joint_goal = group.get_current_joint_values()
		joint_goal[0] = req.Point[0]
		joint_goal[1] = req.Point[1]
		joint_goal[2] = req.Point[2]
		joint_goal[3] = req.Point[3]
		joint_goal[4] = req.Point[4]
		joint_goal[5] = req.Point[5]
		joint_goal[6] = req.Point[6]

		group.go(joint_goal, wait=True)
		group.stop()
		print "Your move on joint space was executed"  	
		status_msg = "Joint Move is Complete"	

		return JointMoveResponse(status_msg)
		
	def task_move(self, req):
		
		group_name=req.GroupName
		if(group_name == "arm_left"): group = self.group_left
		if(group_name == "arm_right"): group = self.group_right

		pose_goal = geometry_msgs.msg.Pose()

		pose_goal.position.x = req.RobotPose.position.x
		pose_goal.position.y = req.RobotPose.position.y
		pose_goal.position.z = req.RobotPose.position.z
		pose_goal.orientation.x = req.RobotPose.orientation.x
		pose_goal.orientation.y = req.RobotPose.orientation.y
		pose_goal.orientation.z = req.RobotPose.orientation.z
		pose_goal.orientation.w = req.RobotPose.orientation.w
		
		group.set_pose_target(pose_goal)
		group.go(wait=True)
		group.stop()
		print "Your move on task space was executed:" 
		group.clear_pose_targets()

		return TaskMoveResponse('Task Move is complete')

	def task_plan(self, req):
		
		group_name=req.GroupName
		if(group_name == "arm_left"): group = self.group_left
		if(group_name == "arm_right"): group = self.group_right
		
		pose_goal = geometry_msgs.msg.Pose()
		
		pose_goal.position.x = req.RobotPose.position.x
		pose_goal.position.y = req.RobotPose.position.y
		pose_goal.position.z = req.RobotPose.position.z
		pose_goal.orientation.x = req.RobotPose.orientation.x
		pose_goal.orientation.y = req.RobotPose.orientation.y
		pose_goal.orientation.z = req.RobotPose.orientation.z
		pose_goal.orientation.w = req.RobotPose.orientation.w

		group.set_pose_target(pose_goal)
		plan = group.plan()
		
		# ADVERTISE: change the pile_path according to path
		file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'plan.yaml')

		with open(file_path, 'w') as file_save:
			yaml.dump(plan, file_save, default_flow_style=True)

		group.stop()
		group.clear_pose_targets()
		print "Your planning on task space is completed" 
		return TaskMoveResponse('Task Plan is complete')

	def task_execute(self, req):
		
		group_name=req.GroupName
		if(group_name == "arm_left"): group = self.group_left
		if(group_name == "arm_right"): group = self.group_right

		#file_name = req.FileName
		file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'plan.yaml')

		with open(file_path, 'r') as file_open:
			loaded_plan = yaml.load(file_open, yaml.FullLoader)

		group.execute(loaded_plan, wait=True)
		print "Your planning on task space was executed" 
		return TaskExecuteResponse('Task Plan is complete')

	def torso_joint(self, req):

		group_name= req.GroupName
		if(group_name == "torso"): group = self.group_torso
		joint_goal = group.get_current_joint_values()
		joint_goal[0] = req.Point[0]

		group.go(joint_goal, wait=True)
		group.stop()
		print "Your move on joint space was executed"  	
		status_msg = "Joint Move is Complete"	

		return JointMoveResponse(status_msg)

	def torso_task(self, req):
		
		group_name= req.GroupName
		if(group_name == "torso"): group = self.group_torso
		x_point = float(req.RobotPose.position.x)
		y_point = float(req.RobotPose.position.y)
		torso_value = group.get_current_joint_values()
		
		torso_value[0] = math.atan2(y_point, x_point)  
		group.go(torso_value, wait=True)
		group.stop()
		print "Your move on task space was executed"  	
		status_msg = "Task Move is Complete"	

		return TaskMoveResponse(status_msg)
		
if __name__ == '__main__':
	lenny_move = PIRLennyMoveServer()

	print "\n"
	print "-------------------------------------------------------------"
	print "Lenny Move Server: Please select some option:"
	print "-------------------------------------------------------------"
	print "Please enter the follow sequence: Movement Robot_Group Points"
	print "For plan execution please enter: Movement Robot_Group"
	print "-------------------------------------------------------------"
	print "Movement:"
	print "	-task_move : for move of a arm in task's space"
	print "	-task_plan : for plan of a arm in task's space"
	print "	-joint_move: for move of a arm in joint's space"
	print "	-task_execute: for execute a saved plan"
	print "-------------------------------------------------------------"
	print "Robot_Group:"
	print "	-arm_left:  Available for task_move, task_plan, joint_move"
	print "	-arm_right: Available for task_move, task_plan, joint_move"
	print "	-torso:     Available for joint_move, task_move"
	print "-------------------------------------------------------------"
	print "Points:"
	print "	-task's points: [X,Y,Z, quat_x, quat_y, quat_z, quat_w]"
	print "	-joint's points: [s, l, e, u, r, b, t]"
	print "-------------------------------------------------------------"
	print "\n"

	JointMoveServer = rospy.Service('pir_joint_move', JointMove, lenny_move.joint_move)
	TaskMoveServer = rospy.Service('pir_task_move', TaskMove, lenny_move.task_move)
	TaskPlanServer = rospy.Service('pir_task_plan', TaskMove, lenny_move.task_plan)
	TaskExecuteServer = rospy.Service('pir_task_execute', TaskExecute, lenny_move.task_execute)
	TorsoJointServer = rospy.Service('pir_torso_joint', JointMove, lenny_move.torso_joint)
	TorsoTaskServer = rospy.Service('pir_torso_task', TaskMove, lenny_move.torso_task)
	rospy.spin()

	
