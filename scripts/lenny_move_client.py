#!/usr/bin/env python

################################################################
#              CTAI-PUJ - Perception In Robotics               #
#                     Date:  26/07/2019                        #
# 									  lenny_move_client                        #
################################################################

import sys
import rospy
from pir_lenny_move.srv import *
import geometry_msgs.msg

def joint_move_client(GroupName, Points):
	rospy.wait_for_service('pir_joint_move')
	try:
		pir_joint_move = rospy.ServiceProxy('pir_joint_move', JointMove)
		resp1 = pir_joint_move(GroupName, Points)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

def task_move_client(GroupName, Point):
	rospy.wait_for_service('pir_task_move')
	try:
		pir_task_move = rospy.ServiceProxy('pir_task_move', TaskMove)
		resp1 = pir_task_move(GroupName, Point)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

def task_plan_client(GroupName, Point):
	rospy.wait_for_service('pir_task_plan')
	try:
		pir_task_plan = rospy.ServiceProxy('pir_task_plan', TaskMove)
		resp1 = pir_task_plan(GroupName, Point)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e
		
def task_execute_client(GroupName):
	rospy.wait_for_service('pir_task_execute')
	try:
		pir_task_execute = rospy.ServiceProxy('pir_task_execute', TaskExecute)
		resp1 = pir_task_execute(GroupName)
		return resp1.status
	except rospy.ServiceExecption, e:
		print "Service call failed: %s"%e

def torso_joint_client(GroupName, Point):
	rospy.wait_for_service('pir_torso_joint')
	try:
		pir_torso_joint = rospy.ServiceProxy('pir_torso_joint', JointMove)		
		resp1 = pir_torso_joint(GroupName, Point)
		return resp1.status
	except rospyServiceException, e:
		print "Service call failed: %s"%e

def torso_task_client(GroupName, Point):
	rospy.wait_for_service('pir_torso_task')
	try:
		pir_torso_task = rospy.ServiceProxy('pir_torso_task', TaskMove)
		resp1 = pir_torso_task(GroupName, Point)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

if __name__ == '__main__':

	# Joints in sda10f robot are:  s,l,e,u,r,b,t
	# Groups avaible: arm_left, arm_right
	# Tasks points in Quaternion
	
	if sys.argv[1] == "joint_move":
		
		my_points = []
		my_group_name = str(sys.argv[2])
		
		if(my_group_name == "arm_left" or my_group_name == "arm_right"):
			for element in sys.argv[3:]:
				my_points.append(float(element))
			print joint_move_client(my_group_name,my_points)
		
		if(my_group_name == "torso"):
			for element in sys.argv[3:]:
				my_points.append(float(element))
			print torso_joint_client(my_group_name,my_points)

	if sys.argv[1] == "task_move" or sys.argv[1] == "task_plan":
		
		poseGoal = geometry_msgs.msg.Pose()
		my_group_name = str(sys.argv[2])
		poseGoal.position.x = float(sys.argv[3])
		poseGoal.position.y = float(sys.argv[4])
		poseGoal.position.z = float(sys.argv[5])
		poseGoal.orientation.x = float(sys.argv[6])
		poseGoal.orientation.y = float(sys.argv[7])
		poseGoal.orientation.z = float(sys.argv[8])
		poseGoal.orientation.w = float(sys.argv[9])
		
		if sys.argv[1] == "task_move":
			
			if my_group_name == "arm_left" or my_group_name == "arm_right":
				print task_move_client(my_group_name, poseGoal)
			
			if my_group_name == "torso":
				print torso_task_client(my_group_name, poseGoal)
		
		if sys.argv[1] == "task_plan":
			print task_plan_client(my_group_name, poseGoal)

	if sys.argv[1] == "task_execute":
		my_group_name = str(sys.argv[2])
		print task_execute_client(my_group_name)

	
