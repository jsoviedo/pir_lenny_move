#!/usr/bin/env python

import sys
import rospy
from pir_lenny_move.srv import *
import geometry_msgs.msg

def move_torso_client(PlanName):
	rospy.wait_for_service('pir_move_torso')
	try: 
		pir_move_torso = rospy.ServiceProxy('pir_move_torso', ReadTrajectory)
		resp1 = pir_move_torso(PlanName)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

def execute_arms_trajectory(PlanName):
	rospy.wait_for_service('pir_execute_arms_trajectory')
	try:
		pir_execute_arms_trajectory = rospy.ServiceProxy('pir_execute_arms_trajectory', ReadTrajectory)
		resp1 = pir_execute_arms_trajectory(PlanName)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

def create_arms_trajectory(PlanName):
	rospy.wait_for_service('pir_create_arms_trajectory')
	try:
		pir_create_arms_trajectory = rospy.ServiceProxy('pir_create_arms_trajectory', ReadTrajectory)
		resp1 = pir_create_arms_trajectory(PlanName)
		return resp1.status
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

if __name__ == '__main__':

	sol_A = "from_home_robot_to_home_pick_bottle"
	sol_B = "from_home_pick_bottle_to_home_pick_tool"
	sol_C = "from_home_pick_tool_to_home_pick_bottle"
	sol_D1 = "from_home_pick_bottle_to_home_place_bottle_1"
	sol_D2 = "from_home_pick_bottle_to_home_place_bottle_2"
	sol_E1 = "from_home_place_bottle_to_home_pick_bottle_1"
	sol_E2 = "from_home_place_bottle_to_home_pick_bottle_2"
	sol_F = "from_home_pick_bottle_to_home_robot"

	# call a service (example):
	# rosrun pir_lenny_move pir_saved_trajectory_client Create_arms_plan Sol_A
	# rosrun pir_lenny_move pir_saved_trajectory_client Execute_arms_plan Sol_A
	# rosrun pir_lenny_move pir_saved_trajectory_client Move_torso Sol_C

	if sys.argv[1] == "Create_arms_plan":
		if sys.argv[2] == "Sol_A" : print(create_arms_trajectory(sol_A))
		if sys.argv[2] == "Sol_D1" : print(create_arms_trajectory(sol_D1))
		if sys.argv[2] == "Sol_D2" : print(create_arms_trajectory(sol_D2))
		if sys.argv[2] == "Sol_E1" : print(create_arms_trajectory(sol_E1))
		if sys.argv[2] == "Sol_E2" : print(create_arms_trajectory(sol_E2))
		if sys.argv[2] == "Sol_F" : print(create_arms_trajectory(sol_F))

	if sys.argv[1] == "Execute_arms_plan":
		if sys.argv[2] == "Sol_A" : print(execute_arms_trajectory(sol_A))
		if sys.argv[2] == "Sol_D1" : print(execute_arms_trajectory(sol_D1))
		if sys.argv[2] == "Sol_D2" : print(execute_arms_trajectory(sol_D2))
		if sys.argv[2] == "Sol_E1" : print(execute_arms_trajectory(sol_E1))
		if sys.argv[2] == "Sol_E2" : print(execute_arms_trajectory(sol_E2))
		if sys.argv[2] == "Sol_F" : print(execute_arms_trajectory(sol_F))		

	if sys.argv[1] == "move_torso":
		if sys.argv[2] == "Sol_C" : print(move_torso_client(sol_C))
		if sys.argv[2] == "Sol_B" : print(move_torso_client(sol_B))




