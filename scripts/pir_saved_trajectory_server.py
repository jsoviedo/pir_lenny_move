#!/usr/bin/env python

# import messages 

from pir_lenny_move.srv import *

# import lybraries

import sys
import copy
import rospy

import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from math import pi
from std_msgs.msg import String

import tf_conversions
import yaml
import os

##################################################################################
#                                  INFO ABOUT POINTS
##################################################################################
# Solution A (ARMS)
# De 1 a 2
#from_home_robot_to_home_pick_bottle

# Solution B (TORSO)
# De 2 a 3
#from_home_pick_bottle_to_home_pick_tool

# Solution C (TORSO)
# De 3 a 2
#from_home_pick_tool_to_home_pick_bottle

# Solution D (ARMS)
# De 2 a 4
#from_home_pick_bottle_to_home_place_bottle

# Solution E (ARMS)
# De 4 a 2
#from_home_place_bottle_to_home_pick_bottle

# Solution F (ARMS)
# De 2 a 1
#from_home_pick_bottle_to_home_robot
###################################################################################

class PIRTrajectoryServer(object):

	def __init__(self):
		super(PIRTrajectoryServer, self).__init__()
		moveit_commander.roscpp_initialize(sys.argv)
		rospy.init_node('pir_trajectory_server', anonymous=True)

		# Create robot and scene

		robot = moveit_commander.RobotCommander()
		scene = moveit_commander.PlanningSceneInterface()

		# Initialize motions groups

		arms_group = moveit_commander.MoveGroupCommander('arms')
		torso_group = moveit_commander.MoveGroupCommander('torso')
		arm_left_group = moveit_commander.MoveGroupCommander('arm_left')
		arm_right_group = moveit_commander.MoveGroupCommander('arm_right')

		display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)

		planning_frame = torso_group.get_planning_frame

		self.robot = robot
		self.scene = scene
		self.display_trajectory_publisher = display_trajectory_publisher
		
		self.arms_group = arms_group
		self.torso_group = torso_group
		self.arm_left_group = arm_left_group
		self.arm_right_group = arm_right_group

# PLANNING_ARMS FUNCTION

	def planning_arms(self, req):

		# Group's Setting

		group = self.arms_group

		group.set_num_planning_attempts(10)
		group.set_goal_position_tolerance(0.01)
		group.set_planning_time(10.0)
		group.set_planner_id("RRTConnectConfigDefault")
		group.allow_replanning(True)
		self.group_arms = group

		# ------------------------------------------------------------------
		# Create Pose Messages
		
		# PICK_BOTTLE_POSE
		pick_bottle_pose_left = geometry_msgs.msg.Pose()
		pick_bottle_pose_right = geometry_msgs.msg.Pose()

		# OVERHEAD_POSE
		overhead_pose_left = geometry_msgs.msg.Pose()
		overhead_pose_right = geometry_msgs.msg.Pose()

		# PLACE_BOTTLE_POSE
		place_bottle_pose_left = geometry_msgs.msg.Pose()
		place_bottle_pose_right = geometry_msgs.msg.Pose()

		# HOME_ROBOT_POSE
		home_robot_pose_left = geometry_msgs.msg.Pose()
		home_robot_pose_right = geometry_msgs.msg.Pose()
		# -------------------------------------------------------------------

		# Pose assignment

		# ******************************************************************

		# PICK_BOTTLE_POSE_LEFT (assign)
		pick_bottle_pose_left.position.x = 0.5
		pick_bottle_pose_left.position.y = 0.4
		pick_bottle_pose_left.position.z = 1.3
		pick_bottle_pose_left.orientation.x = 0.0
		pick_bottle_pose_left.orientation.y = 0.0
		pick_bottle_pose_left.orientation.z = 0.0
		pick_bottle_pose_left.orientation.w = 1.0

		# PICK_BOTTLE_POSE_RIGHT (assign)
		pick_bottle_pose_right.position.x = 0.5
		pick_bottle_pose_right.position.y = -0.4
		pick_bottle_pose_right.position.z = 1.3
		pick_bottle_pose_right.orientation.x = 0.0
		pick_bottle_pose_right.orientation.y = 0.0
		pick_bottle_pose_right.orientation.z = 0.0
		pick_bottle_pose_right.orientation.w = 1.0
		# ******************************************************************

		# OVERHEAD_POSE_LEFT (According to srdf)
		overhead_pose_left.position.x = 0.16
		overhead_pose_left.position.y = 0.32
		overhead_pose_left.position.z = 1.95
		overhead_pose_left.orientation.x = 0.51596
		overhead_pose_left.orientation.y = 0.85213
		overhead_pose_left.orientation.z = 0.082
		overhead_pose_left.orientation.w = -0.030475

		# OVERHEAD_POSE_RIGHT (According to srdf)
		overhead_pose_right.position.x = 0.26
		overhead_pose_right.position.y = -0.32
		overhead_pose_right.position.z = 1.94
		overhead_pose_right.orientation.x = 0.85644
		overhead_pose_right.orientation.y = -0.47955
		overhead_pose_right.orientation.z = 0.00596
		overhead_pose_right.orientation.w = 0.19099

		# PLACE_BOTTLE_POSE_LEFT (assign)
		place_bottle_pose_left.position.x = -0.4
		place_bottle_pose_left.position.y = 0.5
		place_bottle_pose_left.position.z = 1.2
		place_bottle_pose_left.orientation.x = 0.0
		place_bottle_pose_left.orientation.y = 0.0
		place_bottle_pose_left.orientation.z = 0.0
		place_bottle_pose_left.orientation.w = 1.0

		# PLACE_BOTTLE_POSE_RIGHT (assign)
		place_bottle_pose_right.position.x = -0.4
		place_bottle_pose_right.position.y = -0.5
		place_bottle_pose_right.position.z = 1.2
		place_bottle_pose_right.orientation.x = 0.0
		place_bottle_pose_right.orientation.y = 0.0
		place_bottle_pose_right.orientation.z = 0.0
		place_bottle_pose_right.orientation.w = 1.0

		# ******************************************************************
		# According to Rviz:
		#
		# arm_rigth_link_tool_0 = position    (0.19464, -1.3496, 1.2173)
		#                         orientation (0.49689, 0,50348, -0.5023, 0.49729)
		#
		# arm_left_link_tool_0  = position    (0.19253, -1.1196, 1.2175)
		#                         orientation (-0.004097, -0.70681, 0.70738, -0.0041002)	
		#
		# ******************************************************************

		# HOME_ROBOT_POSE_LEFT (assign)
		home_robot_pose_left.position.x = -0.19
		home_robot_pose_left.position.y = 1.1
		home_robot_pose_left.position.z = 1.2
		home_robot_pose_left.orientation.x = 0.0
		home_robot_pose_left.orientation.y = -0.707
		home_robot_pose_left.orientation.z = 0.707
		home_robot_pose_left.orientation.w = 0.0

		# HOME_ROBOT_POSE_RIGHT (assign)
		home_robot_pose_right.position.x = 0.19
		home_robot_pose_right.position.y = -1.3
		home_robot_pose_right.position.z = 1.2
		home_robot_pose_right.orientation.x = 0.0
		home_robot_pose_right.orientation.y = -0.707
		home_robot_pose_right.orientation.z = 0.707
		home_robot_pose_right.orientation.w = 0.0
		# ******************************************************************

		# From A solution
		if (req.name == "from_home_robot_to_home_pick_bottle"):

			# Liks needed for plannig :
			# eef_link_left  = arm_left_link_7_t
			# eef_link_right = arm_right_link_7_t

			group.set_pose_target(pick_bottle_pose_left, "arm_left_link_7_t")
			group.set_pose_target(pick_bottle_pose_right, "arm_right_link_7_t")
			plan_pick = group.plan()
			file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_robot_to_home_pick_bottle.yaml')

		# From E_1 solution 
		if (req.name == "from_home_place_bottle_to_home_pick_bottle_1"):

			group.set_pose_target(overhead_pose_left, "arm_left_link_7_t")
			group.set_pose_target(overhead_pose_right, "arm_right_link_7_t")
			plan_pick = group.plan()
			file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_place_bottle_to_home_pick_bottle_1.yaml')
		
		# From E_2 solution 
		if (req.name == "from_home_place_bottle_to_home_pick_bottle_2"):

			group.set_pose_target(pick_bottle_pose_left, "arm_left_link_7_t")
			group.set_pose_target(pick_bottle_pose_right, "arm_right_link_7_t")
			plan_pick = group.plan()
			file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_place_bottle_to_home_pick_bottle_2.yaml')

		# From F solution
		if (req.name == "from_home_pick_bottle_to_home_robot"):

			group.set_pose_target(home_robot_pose_left, "arm_left_link_7_t")
			group.set_pose_target(home_robot_pose_right, "arm_right_link_7_t")
			plan_pick = group.plan()
			file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_pick_bottle_to_home_robot.yaml')

		# From D_1 solution 
 		if (req.name == "from_home_pick_bottle_to_home_place_bottle_1"):

			group.set_pose_target(overhead_pose_left, "arm_left_link_7_t")
			group.set_pose_target(overhead_pose_right, "arm_right_link_7_t")
			plan_pick = group.plan()
			file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_pick_bottle_to_home_place_bottle_1.yaml')

		# From D_2 solution
		if (req.name == "from_home_pick_bottle_to_home_place_bottle_2"):

			group.set_pose_target(place_bottle_pose_left, "arm_left_link_7_t")
			group.set_pose_target(place_bottle_pose_right, "arm_right_link_7_t")
			plan_pick = group.plan()
			file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_pick_bottle_to_home_place_bottle_2.yaml')	

		with open(file_path, 'w') as file_save:
			yaml.dump(plan_pick, file_save, default_flow_style=True)

		group.stop()
		group.clear_pose_targets()

		status_msg = "SUCCEED"
		return ReadTrajectoryResponse(status_msg)

	# EXECUTE PLANNING ARMS FUCTION

	def execute_arms_planning(self, req):

		group = self.arms_group

		# sol_A
		if (req.name ==  "from_home_robot_to_home_pick_bottle" ): file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_robot_to_home_pick_bottle.yaml')
		# sol_E1
		if (req.name ==  "from_home_place_bottle_to_home_pick_bottle_1" ): file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_place_bottle_to_home_pick_bottle_1.yaml')
		# sol_E2
		if (req.name ==  "from_home_place_bottle_to_home_pick_bottle_2" ): file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_place_bottle_to_home_pick_bottle_2.yaml')
		# sol_F
		if (req.name == "from_home_pick_bottle_to_home_robot"): file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_pick_bottle_to_home_robot.yaml')
		# sol_D1
		if (req.name == "from_home_pick_bottle_to_home_place_bottle_1"): file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_pick_bottle_to_home_place_bottle_1.yaml')
		# sol_D2
		if (req.name == "from_home_pick_bottle_to_home_place_bottle_2"): file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', 'from_home_pick_bottle_to_home_place_bottle_2.yaml')

		#file_path = os.path.join(os.path.expanduser('~'), 'catkin_ws/src/pir_lenny_move/yaml', req.name)
	
		with open(file_path, 'r') as file_open:
			#loaded_plan = yaml.load(file_open, yaml.SafeLoader)
			loaded_plan = yaml.load(file_open, yaml.FullLoader)

		group.execute(loaded_plan, wait=True)
		status_msg = "SUCCEED"
		return ReadTrajectoryResponse(status_msg)

	# MOVE TORSO FUCTION

	def move_torso(self, req):

		group = self.torso_group
		joint_goal = group.get_current_joint_values()

		if (req.name == "from_home_pick_bottle_to_home_pick_tool"):
			joint_goal[0] = 1.517
		
		if (req.name == "from_home_pick_tool_to_home_pick_bottle"):
			joint_goal[0] = 0.0

		group.go(joint_value, wait=True)
		group.stop()

		status_msg = "SUCCEED"
		return ReadTrajectoryResponse(status_msg)

if __name__ == '__main__':

	lenny_move_pkg = PIRTrajectoryServer()

	print("------------------------ Service available ----------------------------")
	print("1- Create_arms_plan")
	print("2- Execute_arms_plan")
	print("3- Move_torso")
	print("------------------------- Solutions -----------------------------------")
	print("sol_A = from_home_robot_to_home_pick_bottle        (Arms)")
	print("sol_B = from_home_pick_bottle_to_home_pick_tool    (Torso)")
	print("sol_C = from_home_pick_tool_to_home_pick_bottle    (Torso)")
	print("sol_D = from_home_pick_bottle_to_home_place_bottle (Arms)")
	print("sol_E = from_home_place_bottle_to_home_pick_bottle (Arms)")
	print("sol_F = from_home_pick_bottle_to_home_robot        (Arms)")
	print("-----------------------------------------------------------------------")

	CreateArmsTrajectoryServer = rospy.Service('pir_create_arms_trajectory', ReadTrajectory, lenny_move_pkg.planning_arms)
	MoveTorsoServer = rospy.Service('pir_move_torso', ReadTrajectory, lenny_move_pkg.move_torso)
	ExecuteArmsTrajectoryServer = rospy.Service('pir_execute_arms_trajectory', ReadTrajectory, lenny_move_pkg.execute_arms_planning)
	rospy.spin()

		
